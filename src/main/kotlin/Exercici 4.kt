import kotlinx.coroutines.*
import java.util.Scanner
import kotlin.system.exitProcess

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

/*
Crea una aplicació en que l’usuari hagi d’encertar un número secret entre 1 i 50 que es generarà aleatòriament.
L’usuari anirà introduint valors fins que encerti el número.
L’aplicació tindrà un compte enrere de 10 segons. Passat aquest temps, l’aplicació finalitzarà.

 */

fun main() = runBlocking {
    val scanner = Scanner(System.`in`)
    val numeroSecreto = (1..50).random() //Generamos número random
    println("Encuentra el número secreto entre el 1 y 50")
    println("¡Tienes 10 segundos para encontrarlo!")

    cuentaAtras()
    encontrarNumero(numeroSecreto, scanner)
}

fun encontrarNumero(numeroSecreto : Int, scanner:Scanner) {
    do {
        println("Introduzca un número: ")
        val numero = scanner.nextInt()
        if (numero == numeroSecreto) println("¡Lo has encontrado!")
        else println("Te has equivocado..")
    } while (numero != numeroSecreto)
}

@OptIn(DelicateCoroutinesApi::class)
fun cuentaAtras() {
    GlobalScope.launch {
        delay(10000)
        println("¡Se ha acabado el tiempo!")
        delay(100)
        println("¡Hasta la próxima!")
        exitProcess(0)
    }
}