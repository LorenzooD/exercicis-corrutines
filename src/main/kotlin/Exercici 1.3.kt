import kotlinx.coroutines.*

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

fun main() = runBlocking {
    println("The main program is started")
    withContext(Dispatchers.Default) {
        doBackground2()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground2() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}