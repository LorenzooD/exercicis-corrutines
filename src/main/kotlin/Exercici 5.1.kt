import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

/*
    * Canvia el codi anterior perquè les dues corrutines s’executin alhora.
*/



fun main() {
    runBlocking {
        coroutineScope {
            launch { corroutine3() }
            launch { corroutine4() }
        }
    }
    println("Completed")
}
private suspend fun corroutine3() {

    println("Hello World 1.1")
    delay(3000)
    println("Hello World 1.2")
    delay(3000)
    println("Hello World 1.3")
}

private suspend fun corroutine4() {

    println("Hello World 2.1")
    delay(2000)
    println("Hello World 2.2")
    delay(2000)
    println("Hello World 2.3")
}
