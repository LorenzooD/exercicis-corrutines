import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}
