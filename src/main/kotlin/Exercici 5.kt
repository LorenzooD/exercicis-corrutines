import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

/*
    * Executa el següent codi i mira què passa:
    * Primer crida la funció corroutine1() y després quan s'acaba, s'executa la funció corroutine2(), perqué les dues funcions es criden dins de runBlocking
 */

fun main() {
    runBlocking {
        corroutine1()
        corroutine2()
    }
    println("Completed")
}

suspend fun corroutine1() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}

suspend fun corroutine2() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}

/*
    Printa lo siguiente:
    Hello World 1.1
    Hello World 1.2
    Hello World 1.3
    Hello World 2.1
    Hello World 2.2
    Hello World 2.3
    Completed
 */
