import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

/*
Crea un programa que simula una cursa de cavalls.
Cada cavall serà una corrutina, que ha de completar un bucle de 4 iteracions en que imprimirà un missatge indicant en quin punt del circuit està el cavall. Cada missatge es mostrarà amb un delay aleatori. En acabar aquest bucle, s’ha de mostrar un missatge indicant que el cavall ha acabat la cursa.

 */
fun main() {
    println("¡Comienza la carrera!")

    val podium = mutableListOf<String>()

    runBlocking {
        launch { horse("🐴", podium) }
        launch { horse("🦓", podium) }
        launch { horse("🦄", podium) }
        launch { horse("🐎", podium) }
        launch { horse("🐴", podium) }
    }

    println("¡Ha acabado la carrera!")
    println()
    println("Podio:")
    podium.forEachIndexed { index, horse ->
        println("${index + 1} lugar: $horse")
    }
}

suspend fun horse(icon: String, podium: MutableList<String>) {
    repeat(4) { currentIteration ->
        val distance = "   ".repeat(currentIteration) + icon
        println("$distance")
        delay((1000L..5000L).random())
    }
    println("$icon ha acabado la carrera")
    podium.add(icon)
}


