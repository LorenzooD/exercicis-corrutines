import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1500)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(1000)
        println("The main program is finished")
    }
}

/*
    * The main program is started
    * The main program continues
    * Background processing started
    * The main program is finished

 */