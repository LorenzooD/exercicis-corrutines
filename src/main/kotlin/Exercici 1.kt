import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
 */

/*
    * Quina sortida generarà el següent codi?
 */


fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

/*
    1. Función "println" para imprimir un mensaje por consola

    2. Función "GlobalScope.launch" para lanzar un nuevo hilo. La tarea imprime
    un mensaje mediante println, espera 1000 milisegundos utilizando "delay", y
    luego imprime un mensaje que indica que el procesamiento ha finalizado.

    3. Función "println" para imprimir un mensaje por consola

    4. La función "runBlocking" se utiliza para bloquear el hilo principal del
    programa hasta que todas las tareas en segundo plano se completen. En este caso,
    espera 1500 milisegundos utilizando "delay" y luego imprime un mensaje que indica
    que el programa principal ha finalizado.

    * La salida del programa sería:
    * The main program is started
    * The main program continues
    * Background processing started
    * Background processing finished
    * The main program is finished

 */
