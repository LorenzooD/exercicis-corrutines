import kotlinx.coroutines.*
import java.util.Scanner

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
*/

/*
Fes un programa en que la funció main cridi a una funció que escrigui per pantalla el missatge “Hello World!”
precedit pel número de missatge. Desprès d’escriure cada missatge espera 100 mil·lisegons.
En acabar la funció, torna al main que escriu per pantalla el missatge “Finished!”.
Fes dues versions del programa: una en que el launch es trobi al main i una altra dins de la funció.
 */


// VERSION 1
fun main() = runBlocking {
    val scanner = Scanner(System.`in`)
    println("Introduce el número de veces que quieres ver 'Hello World!'")
    val input = scanner.nextInt()

    mensaje(input)
    delay(100)
    println("Finalizado")
}

suspend fun mensaje(input : Int) {
    coroutineScope {
        for (i in 0 until input) {
            launch {
                delay(100)
                println("Hello World!")
            }
        }
    }
}



//// VERSION 2
//fun main() {
//    val scanner = Scanner(System.`in`)
//
//    runBlocking {
//        println("Cuantas veces quieres que escriba 'Hello World!' ? ")
//        val input = scanner.nextInt()
//        mensaje(input)
//    }
//    println("Finalizado")
//}
//
//suspend fun mensaje(input : Int) {
//    coroutineScope {
//        for (i in 1..input) {
//            launch {
//                println("Hello World!")
//            }
//            delay(100)
//        }
//    }
//}



