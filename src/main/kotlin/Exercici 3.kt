import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.system.measureTimeMillis

/*
    * Lorenzo Poderoso Dalmau
    * DATE: 16/04/2023
*/

/*
Crea una corrutina que reprodueixi una cançó. Entenem per “reproduir” anar mostrant la lletra per estrofes d’una
cançó amb 3 segons de retard entre línia i línia. La lletra es llegirà d’un arxiu.
L’aplicació principal mostrarà el temps transcorregut en l’execució de l’aplicació. Per fer-ho, has d’incloure el
 codi que vulguis mesurar dins del següent fragment:
 */

fun main() {
    val time = measureTimeMillis {
        val song = File("song.txt")
        val songText = song.readLines()

        for (line in songText) {
            if (line == "") {
                runBlocking {
                    delay(3000)
                }
            }
            println(line)
        }
    }

    println("Tiempo : $time milisegundos")
}